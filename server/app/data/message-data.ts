import { Message } from "../models/models";

export class MessageQueue{

    readonly data: Message[] = [];

    addMessage(message: Message){
        this.data.push(message);
    }

    removeMessage(){

    }

    getMessagesFor(licenseNumber: string) {
        const messages: Message[] = [];
        const idxToDel = [];
        let idx = 0;
        for(let message of this.data) {
            if(message.toDeviceLicense === licenseNumber){
                message.status = "delivered";
                idxToDel.push(idx);
                messages.push(message);
            }
        }
        this.deleteMessagesWithIndes(idxToDel.reverse());
        return messages;
    }

    deleteMessagesWithIndes(indexes: number[]){
        for(const idx of indexes){
            this.data.splice(idx, 1);
        }
    }

    getAllMessages(){

    }

}