import { Group, Device, Message } from "../models/models";
import { DataService } from "./dataService";

export class GroupData{

    readonly data: Group[] = [];

    addGroup(group: Group){
        this.data.push(group);
    }

    getGroups(){
        return this.data;
    }

    getDeviceGroups = (deviceLicense :string)=>{
        let userGroups: string[] = [];
        for(let i=0; i<this.data.length; ++i) {
            const group: Group = this.data[i];
            if(group.deviceLicense.indexOf(deviceLicense)!== -1)
            {
                userGroups.push(group.name);
            }
        }
        return userGroups;
    }
    
    getGroupWithName = (groupName :string)=>{
        for(let i=0; i<this.data.length; ++i) {
            if(this.data[i].name === groupName){
                return this.data[i];
            }
        }
        return null;
    }

    groupExists = (groupName: string ) => {
        for(let i=0; i<this.data.length; ++i) {
            if(this.data[i].name === groupName)
                return true;
        }
        return false;
    }
    
    deleteGroup = (groupName: string ) => {
        for(let i = 0; i < this.data.length; ++i){
            if(this.data[i].name === groupName){
                this.data.splice(i, 1);
                return;
            }
        }
    }
 
    addToGroup = (groupName: string, deviceLicense: string) => {
        if(!this.groupExists(groupName)){
            return false;
        }

        for(let i = 0; i < this.data.length; ++i){
            if(this.data[i].name === groupName){
            for(let vi = 0; vi < this.data[i].deviceLicense.length; ++vi){
                if(this.data[i].deviceLicense[vi] === deviceLicense){
                    return false;
                }
            }
            this.data[i].deviceLicense.push(deviceLicense);
            return true;
            }
        }
        return false;
    }

     removeFromGroup = (groupName: string, deviceLicense: string) => {
        if(!this.groupExists(groupName)){
            return false;
        }

        for(let i = 0; i < this.data.length; ++i){
            if(this.data[i].name === groupName){
                for(let vi = 0; vi < this.data[i].deviceLicense.length; ++vi){
                    if(this.data[i].deviceLicense[vi] === deviceLicense){
                        this.data[i].deviceLicense.splice(vi, 1);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    sendMessage = (groupName: string, fromDeviceId: string, message: string) => {
        const groupExists = this.groupExists(groupName);
        const deviceExists = DataService.devices.isDeviceWithId(parseInt(fromDeviceId));
        const messages = DataService.messages;
        const devices = DataService.devices;
        
        if( !groupExists || !deviceExists ){
            return false;
        }

        for(let i = 0; i < this.data.length; ++i) {
            if(this.data[i].name === groupName){
                const device: any = devices.findDeviceWithId(parseInt(fromDeviceId));
                const timeCreated = new Date();
                for(let vi = 0; vi < devices.getDevices().length; ++vi){
                    if(this.data[i].deviceLicense === device.deviceLicense)
                        continue;
                    if(this.data[i].deviceLicense.indexOf(devices.data[vi].deviceLicense) === -1 )
                        continue
                    let msg: Message = new Message();
                    msg.fromDeviceLicense = device.deviceLicense;
                    msg.toDeviceLicense = devices.data[vi].deviceLicense;
                    msg.message = message;
                    msg.status='created';
                    msg.timeCreated = timeCreated;
                    messages.addMessage(msg);
                }   
            }
        }
        return true;
    }
}