import { Device } from "../models/models";

export class DeviceData{

    data: Device[] = [];
    
    public getDevices(){
        return this.data;
    }
    public setDevices(devices: Device[]){
        return this.data = devices;
    }

    public getNextId(): number{
        let maxId = 1;
        for(const device of this.data) {
            if(maxId <= device.id) {
                maxId = device.id + 1;
            }
        }
        return maxId;
    }

    public addDevice(device: Device){
        if(this.isDeviceWithLicense(device.deviceLicense)){
            throw Error("Device already exists!");
        }
        this.data.push(device);
        return device;
    }

    public isDeviceWithLicense(license: string){
        for(let device of this.data){
            if(device.deviceLicense === license){
                return true;
            }
        }
        return false;
    }

    public isDeviceWithId(id: number){
        for(let device of this.data){
            if(device.id === id){
                return true;
            }
        }
        return false;
    }

    public removeDevice(id: number){
        if(this.isDeviceWithId(id)){
            this.data.splice(this.getIndexOfDevice(id), 1);
        }
    }


    public getIndexOfDevice(id: number): number{
        let idx = 0;
        for(let v of this.data){
            if(v.id === id){
                return idx;
            }
            ++idx;
        }
        return -1;
    }

    public findDeviceWithLicense(license: string){
        for(let v of this.data){
            if(v.deviceLicense === license){
                return v;
            }
        }
        return null;
    }
    public findDeviceWithId(id: number){
        for(let v of this.data){
            if(v.id == id){
                return v;
            }
        }
        return null;
    }

    public heartBeatReceived(id: number){
        for(let device of this.data){
            if(device.id == id){
                device.lastHeartBeat = new Date();
                return device;
            }
        }
        return null;
    }

}