import { DeviceData } from "./device-data";
import { MessageQueue } from "./message-data";
import { GroupData } from "./group-data";


export class DataService {
    public static readonly devices: DeviceData = new DeviceData();
    public static readonly messages: MessageQueue = new MessageQueue();
    public static readonly groups: GroupData = new GroupData();

    public static readonly LOG_FILE_NAME = 'logs.txt';
}