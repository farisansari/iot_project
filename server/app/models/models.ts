
export class Position{
    constructor(pos: any = null){
        this.lat = pos ? pos['lat'] : 0;
        this.lng = pos ? pos['lng'] : 0;
    }
    lat: number =0;
    lng: number =0;
}

export class Device{
    id: number = -1;
    deviceLicense: string = '';
    type: string = '';
    optionalName: string = '';
    socketInfo: string = '';
    lastHeartBeat: any;
    currPos: Position = {lat: 0, lng: 0};
    destPos: Position = {lat: 0, lng: 0};
}

export class Message{
    fromDeviceLicense: string = '';
    toDeviceLicense: string = '';
    message: string = '';
    timeCreated: any;
    status: string = '';
}

export class Group{
    name: string = '';
    deviceLicense: string[] =[]
}