import { Router, Request, Response, json } from 'express';
import { MessageQueue } from '../data/message-data';
import { Group } from '../models/models';
import { DataService } from '../data/dataService';
import { GroupData } from '../data/group-data';
import { DeviceData } from '../data/device-data';

const router: Router = Router();

const groups: GroupData = DataService.groups;
const devices: DeviceData = DataService.devices;

router.get('/showGroup/:groupName', function(req, res) {
    if(groups.groupExists(req.params.groupName)){
        const group = groups.getGroupWithName(req.params.groupName);
        return res.status(200).json({ "group": group});
    } else {
        return res.status(400).json({"Error": "Group with name: " + req.params.groupName + " does not exists"});
    }
});

router.get('/createGroup/:groupName', function(req, res){
    if(!groups.groupExists(req.params.groupName)){
        const group: Group = new Group();
        group.name = req.params.groupName;
        group.deviceLicense = []
        groups.addGroup(group);
        return res.status(200).json({"Message": "Group created!", "group": group});
    } else {
        return res.status(400).json({"Error": "Group with name: " + req.params.groupName + " already exists"});
    }
});

router.get('/deleteGroup/:groupName', function(req, res){
    if(groups.groupExists(req.params.groupName)){
        groups.deleteGroup(req.params.groupName);
        return res.status(200).json({"Message": "Group deleted!"});
    } else {
        return res.status(400).json({"Error": "Group with name: " + req.params.groupName + " does not exist"});
    }
});

router.post('/addDeviceToGroup', function(req, res){
    const groupExists = groups.groupExists(req.body.groupName);
    const deviceExists = devices.isDeviceWithLicense(req.body.licenseNumber);
    if(!deviceExists){
        return res.status(400).json({"Error": `Device with licenseNumber: ${req.body.licenseNumber} not found!`});
    } else if (!groupExists) {
        return res.status(400).json({"Error": `Group with name: ${req.body.groupName} not found!`});
    } else {
        if(groups.addToGroup(req.body.groupName, req.body.licenseNumber))
            return res.status(200).json({"Message": `device: ${req.body.licenseNumber} added to Group: ${req.body.groupName}`});
        return res.status(400).json({"Error": 'Failed to add device'});
    }
});

router.post('/removeDeviceFromGroup', function(req, res){
    const groupExists = groups.groupExists(req.body.groupName);
    const deviceExists = devices.isDeviceWithLicense(req.body.licenseNumber);
    if(!deviceExists) {
        return res.status(400).json({"Error": `Device with licenseNumber: ${req.body.licenseNumber} not found!`});
    } else if (!groupExists) {
        return res.status(400).json({"Error": `Group with groupName: ${req.body.groupName} not found!`});
    } else {
        if(groups.removeFromGroup(req.body.groupName, req.body.licenseNumber))
            return res.status(200).json({"Message": `device: ${req.body.licenseNumber} removed from Group: ${req.body.groupName}`});   
        return res.status(400).json({"Error": 'Error deleting device from group'});   
    }
});

router.post('/sendMessageToGroup', function(req, res){
    const groupExists = groups.groupExists(req.body.groupName);
    const deviceExists = devices.isDeviceWithId(req.body.fromDeviceId);
    if(!deviceExists) {
        return res.status(400).json({"Error": `Device with fromDeviceId: ${req.body.deviceId} not found!`});
    } else if (!groupExists) {
        return res.status(400).json({"Error": `Group with name: ${req.body.groupName} not found!`});
    } else {
        if(groups.sendMessage(req.body.groupName, req.body.fromDeviceId, req.body.message))
            return res.status(200).json({"Message": `Message sent to group: ${req.body.groupName}`});   
        return res.status(400).json({"Error": 'Error sending message to group'});   
    }
});
 
 export const GroupRouter: Router = router;
