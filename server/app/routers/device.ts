import { Router, Request, Response, json } from 'express';
import { DeviceData } from '../data/device-data';
import { Device, Message } from '../models/models';
import { MessageQueue } from '../data/message-data';
import { DataService } from '../data/dataService';
import { GroupData } from '../data/group-data';
import * as FS from 'fs';


const router: Router = Router();
let devices: DeviceData = DataService.devices;
let messages: MessageQueue = DataService.messages;
let groups: GroupData = DataService.groups;
let LOG_FILE_NAME = DataService.LOG_FILE_NAME;

let createDevice = (req: any) => {
    let device: Device = new Device();
    device.optionalName = req.body.optionalName;
    device.deviceLicense = req.body.deviceLicense;
    device.currPos = req.body.currPos;
    device.destPos = req.body.destPos;
    device.socketInfo = req.body.socketInfo == undefined ? 
        (req.headers['x-forwarded-for'] || req.connection.remoteAddress) :
        req.body.socketInfo;
    device.lastHeartBeat = new Date();
    device.id = devices.getNextId();
    device.type = req.body.type;
    return device;
}

let isValidField = (field: any) => {
    if(field !== undefined && field !== null && field !== '')
        return true;
    return false;
}

let isValidDevice = (device: Device) => {
    
    if(! isValidField(device.deviceLicense))
        return false;
    if(! isValidField(device.socketInfo))
        return false;
    if(! isValidField(device.type))
        return false;

    return true;
}


router.post('/registerDevice', function(req, res){
    let device: Device = createDevice(req);
    try{
        if(isValidDevice(device)){
            let result: any = devices.addDevice(device);
            return res.status(200).json({"message": 'Device registered', "device":  result});
        } else {
            return res.status(400).json({"error": 'invalid device!', 'sample':  new Device()});
        }
    } catch(err) {
        return res.status(400).json({"Error": err.message});
    }
 });

 router.get('/unregisterDevice/:id', function(req, res){
    let device: any = devices.findDeviceWithId(req.params.id);

    if(device !== null){
        devices.removeDevice(device.id);
        return res.status(200).json({"message": "device unregistered", "device": device});
    } else {
        return res.status(400).json({"error": "device not found!"});
    }
 });

 router.post('/sendMessage', function(req, res){
    let deviceMsgFrom: any = devices.findDeviceWithId(req.body.id);
    let deviceMsgTo: any = devices.findDeviceWithLicense(req.body.deviceLicense);
    let msg = req.body.message;
    if(deviceMsgFrom !== null && deviceMsgTo !== null && msg != undefined) {
        let message: Message = new Message();
        message.fromDeviceLicense = deviceMsgFrom.deviceLicense;
        message.toDeviceLicense = deviceMsgTo.deviceLicense;
        message.status = "created";
        message.message = msg;
        message.timeCreated = new Date();
        messages.addMessage(message);
        return res.status(200).json({"message": message});
    } else {
        return res.status(400).json({"error": "Invalid request!"});
    }
 });

 router.get('/heartBeat/:id', function(req, res){
    let device: any = devices.heartBeatReceived(req.params.id);
    if(device !== null){
        let msgs = messages.getMessagesFor(device.deviceLicense);
      return res.status(200).json({"device": device, messages: msgs});
    }
    return res.status(400).json({"error": "Invalid request!"});
 });

 router.get('/getDevice', function(req, res){
     let result = devices.getDevices();
     return res.status(200).json({result: result});
 });

 router.get('/getDevice/:id', function(req, res){
    const device: any = devices.findDeviceWithId(req.params.id);
    if(device !== null) {
        return res.status(200).json({device: device});
    } else {
        return res.status(400).json({"error": "Invalid request!"});
    }
 });

 router.get('/getDeviceGroups/:license', function(req, res){
    const device: any = devices.findDeviceWithLicense(req.params.license);
    if(device !== null) {
        let userGroups = groups.getDeviceGroups(req.params.license);
        return res.status(200).json({groups: userGroups});
    } else {
        return res.status(400).json({"error": "Device not found!"});
    }
 });

 router.post('/log', function(req, res){
    let deviceMsgFrom: any = devices.findDeviceWithId(req.body.id);
    let msg = req.body.message;
    if(deviceMsgFrom !== null && msg != undefined) {
        const date: Date = new Date();
        let strDate = date.getUTCDate() + '-' + date.getUTCMonth() + '-' + date.getUTCFullYear() +  
                     ' ' + date.getUTCHours() +':'+ date.getUTCMinutes()+':'+date.getUTCSeconds() + '.' + date.getMilliseconds(); 
        let message: string = 'DATE_TIME::' + strDate + 
         ' FROM::LICENSE:: [' + deviceMsgFrom.deviceLicense + ']: ' + msg + '\n';
        FS.appendFileSync(LOG_FILE_NAME, message);
        return res.status(200).json({"message logged": message});
    } else {
        return res.status(400).json({"error": "Invalid request!"});
    }
 });

 export const DeviceRouter: Router = router;
