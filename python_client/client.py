import http.client
import json

server = 'localhost'
PORT = 3000
conn = http.client.HTTPConnection(server, PORT);
headers = {'Content-type': 'application/json'}


busAPI = 'http://localhost:3000/api/bus' # Bus API
busStopAPI = '/api/busStop' # Bus Stop API
gAPI = '/api/g_api' # Google API

#-----------------------------
def getRequest(route, param=None):
	global conn
	conn.request("GET", route, json.dumps(param), headers)
	response = conn.getresponse()
	return (response.status, response.read())

def postRequest(route, param=None):
	global conn
	conn.request("POST", route, json.dumps(param), headers)
	response = conn.getresponse()
	return (response.status, response.read())


#-----------------------------
def registerBus(busReg):
	global busAPI
	url = busAPI + '/registerBus'
	response = postRequest(url, busReg)
	print('status: ', response[0])
	return response[1]

def getBus(busReg):
	global busAPI
	url = busAPI + '/getBus'
	(status, response) = getRequest(url, busReg);
	print('status: ', response[0])
	return response[1]

#-----------------------------

id = 0

id = id + 1
bus = {
	"name": "W21",
	"id": id
}
result = registerBus(bus)
print(result)

id = id + 1
bus = {
	"name": "F11",
	"id": id
}
result = registerBus(bus)
print(result)

result = getBus({"id": 1})
print(result)
