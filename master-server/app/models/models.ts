
export class Pos{
    constructor(pos: any = null){
        this.lat = pos ? pos['lat'] : 0;
        this.lng = pos ? pos['lng'] : 0;
    }
    lat: number =0;
    lng: number =0;
}

export class Bus{
    id: number = 0;
    name: string = "";
    currPos: Pos = new Pos();
    nextStop: Pos = new Pos();
}

export class BusStop{
    id: number = 0;
    currPos: Pos = new Pos();
    name: string = "";
    busses: Bus[] = [];
}

export class BusRoute{
    id: number = 0;
    name: string = "";
    busStops : BusStop[] = [];
}
