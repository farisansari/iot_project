import { Router, Request, Response } from 'express';

const router: Router = Router();

 router.get('/getDistanceMatrix', function(req, res){
    res.send('Distance matrix for two points');
 });

 
 export const GoogleAPIRouter: Router = router;