import * as Express from 'express';
import * as bodyParser from 'body-parser';
import { GoogleAPIRouter } from './routers/google-api';

export class Application{

    readonly app: Express.Application = Express.default();
    port: any = process.env.PORT;

    constructor(){
        this.configure();
    }

    logger = (req: Express.Request, res: Express.Response, next: any)=> {
        console.log(`${req.method}: "${req.url}" ========================`)
        console.log('body: ', req.body)
        console.log('params: ', req.params)
        //console.log('data: ', req.data)
        console.log('----------------------------');
        next(); // Passing the request to the next handler in the stack.
    }

    configure(){
        this.app.use(bodyParser.json());
        this.app.use(this.logger);
        
        this.app.use("/googleAPI", GoogleAPIRouter);
    }

    public startServer(port: any){
        if(port)
            this.port = port;
        this.app.listen(this.port, () => {
            // Success callback
            console.log(`Listening at http://localhost:${port}/`);
        });
    }    
}

